<?php

namespace GESTION\TiendaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use GESTION\GestionBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\Collections\ArrayCollection;
use GESTION\GestionBundle\Entity\Cliente;
use GESTION\GestionBundle\Form\ClienteType;
use GESTION\GestionBundle\Entity\Colaboracion;
use GESTION\GestionBundle\Entity\ElementoColaboracion;
use GESTION\GestionBundle\Entity\ElementoStock;

class DefaultController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

    public function indexAction()
    {
		if(!$this->sessionManager->isStarted()){
			$this->sessionManager->startSession();
		}
		$em = $this->getDoctrine()->getManager();
		$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy(array('visible'=>true, 'cod_estado'=>'A', 'destacado'=>true));
        return $this->render('GESTIONTiendaBundle:Default:index.html.twig', array('productos'=>$productos));
	}

    public function limpiarAction()
    {
		$this->sessionManager->limpiar();
		return $this->redirect($this->generateUrl('_index'));
	}

    public function contactoAction()
    {
        return $this->render('GESTIONTiendaBundle:Default:contacto.html.twig', array('title'=>'Molate Home - Contacto'));
	}

    public function nosotrosAction()
    {
        return $this->render('GESTIONTiendaBundle:Default:nosotros.html.twig', array( 'title'=>'Molate Home - Nosotros'));
	}

    public function categoriaAction($slug, Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$orden = $request->get('orden');
		$categoria = $em->getRepository('GESTIONGestionBundle:Categoria')->findOneBy(array('slug'=>$slug));

        if(!isset($orden) or $orden == 'defecto'){
			$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy(array('categoria'=>$categoria->getId(), 'visible'=>true, 'cod_estado'=>'A'), array('destacado'=>'DESC'));
		}else{
			if($orden == 'preciomayormenor'){
				$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy(array('categoria'=>$categoria->getId(), 'visible'=>true, 'cod_estado'=>'A'), array('precio'=>'DESC'));
			}
			if($orden == 'preciomenormayor'){
				$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy(array('categoria'=>$categoria->getId(), 'visible'=>true, 'cod_estado'=>'A'), array('precio'=>'ASC'));
			}
		}

        return $this->render('GESTIONTiendaBundle:Default:categoria.html.twig', array('categoria'=>$categoria, 'productos'=>$productos, 'title'=>'Molate Home - '.$categoria->getNombre()));
	}

    public function productoAction($slug)
    {
		$em = $this->getDoctrine()->getManager();
		$producto = $em->getRepository('GESTIONGestionBundle:Elemento')->findOneBy(array('slug'=>$slug));
		$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy(array('categoria'=>$producto->getCategoria()->getId(), 'visible'=>true, 'cod_estado'=>'A'), array('destacado'=>'DESC'));
		if($producto){
			return $this->render('GESTIONTiendaBundle:Default:producto.html.twig', array('producto'=>$producto, 'productos'=>$productos, 'title'=>'Molate Home - '.$producto->getNombre()));
		}else{
			return $this->redirect($this->generateUrl('_index'));
		}
	}

    public function novedadesAction(Request $request)
    {
		$orden = $request->get('orden');
		$em = $this->getDoctrine()->getManager();
        if(!isset($orden) or $orden == 'defecto'){
			$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy(array('novedad'=>true, 'visible'=>true, 'cod_estado'=>'A'), array('destacado'=>'DESC'));
		}else{
			if($orden == 'preciomayormenor'){
				$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy(array('novedad'=>true, 'visible'=>true, 'cod_estado'=>'A'), array('precio'=>'DESC'));
			}
			if($orden == 'preciomenormayor'){
				$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy(array('novedad'=>true, 'visible'=>true, 'cod_estado'=>'A'), array('precio'=>'ASC'));
			}
		}

        return $this->render('GESTIONTiendaBundle:Default:novedades.html.twig', array('productos'=>$productos, 'title'=>'Molate Home - Novedades'));
	}

    public function tiendaAction(Request $request, $categoria)
    {
		$orden = $request->get('orden');

		$em = $this->getDoctrine()->getManager();
		$criterio = array('cod_estado'=>'A', 'visible'=>true);
        if($categoria!='todo'){
			$categoria = $em->getRepository('GESTIONGestionBundle:Categoria')->findOneBy(array('slug'=>$categoria));
			if($categoria){
				$criterio = array('cod_estado'=>'A', 'visible'=>true, 'categoria'=>$categoria->getId());
			}
		}
		if(!isset($orden) or $orden == 'defecto'){
			$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy($criterio, array('destacado'=>'DESC'));
		}else{
			if($orden == 'preciomayormenor'){
				$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy($criterio, array('precio'=>'DESC'));
			}
			if($orden == 'preciomenormayor'){
				$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->findBy($criterio, array('precio'=>'ASC'));
			}
			if($orden == 'categoria'){
				$productos = $em->getRepository('GESTIONGestionBundle:Elemento')->getProducts();
			}
		}

		$categorias = $em->getRepository('GESTIONGestionBundle:Categoria')->findBy(array('cod_estado'=>'A'), array('nombre'=>'ASC'));

        return $this->render('GESTIONTiendaBundle:Default:tienda.html.twig', array('productos'=>$productos, 'categorias'=>$categorias, 'title'=>'Molate Home - Tienda'));
	}

	public function ingresarAction()
	{
		return $this->render('GESTIONTiendaBundle:Default:ingreso.html.twig', array('title'=>'Molate Home - Ingresar'));
	}

	public function loginAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$mail = $request->get('email');
		$clave = $request->get('clave');
		$cliente = $em->getRepository('GESTIONGestionBundle:Cliente')->findOneBy(array('mail'=>$mail, 'clave'=>$clave));
		if($cliente){
			$this->sessionManager->setSession('cliente', $cliente);
			return $this->redirect($this->generateUrl('_index'));
		}else{
			$this->sessionManager->addFlash('msgError', 'Error en los datos ingresados');
			return $this->redirect($this->generateUrl('_ingresar'));
		}
	}

	public function salirAction()
	{
		$this->sessionManager->setSession('cliente', null);
		return $this->redirect($this->generateUrl('_index'));
	}

	public function registroAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$mail = $request->get('email');
		$clave = $request->get('clave');
		$telefono = $request->get('telefono');
		$nombre = $request->get('nombre');

		$cliente = $em->getRepository('GESTIONGestionBundle:Cliente')->findOneBy(array('mail'=>$mail, 'clave'=>$clave));

		if($cliente != null){
			$this->sessionManager->addFlash('msgError', 'Ya existe un usuario con ese EMAIL y esa CLAVE');
			return $this->redirect($this->generateUrl('_ingresar'));
		}else{
			$cliente = new Cliente();
			$cliente->setRazonSocial($nombre);
			$cliente->setTelefono($telefono);
			$cliente->setMail($mail);
			$cliente->setClave($clave);

			$em->persist($cliente);
			$em->flush();

			$this->sessionManager->setSession('cliente', $cliente);
			$this->sessionManager->addFlash('msgInfo', 'BIENVENIDO A MOLATE');
		}

		return $this->redirect($this->generateUrl('_index'));
	}

	public function misdatosAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$cliente = $this->sessionManager->getSession('cliente');
		if(!$cliente){
			return $this->redirect($this->generateUrl('_index'));
		}
        $editForm = $this->createEditForm($cliente);

        return $this->render('GESTIONTiendaBundle:Default:editarcliente.html.twig', array(
            'entity'      => $cliente,
            'edit_form'   => $editForm->createView(),
        ));
	}

	private function createEditForm(Cliente $entity)
    {
        $form = $this->createForm(new ClienteType(), $entity, array(
            'action' => $this->generateUrl('_update_cliente', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=> array('class'=>'btn btn-lg btn-primary')));

        return $form;
    }
	public function updateClienteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Cliente')->find($id);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()){
            $em->flush();

			$this->sessionManager->setSession('cliente', $entity);

			$this->sessionManager->addFlash('msgInfo', 'DATOS MODIFICADOS');
            return $this->redirect($this->generateUrl('_misdatos'));
        }

		return $this->render('GESTIONTiendaBundle:Default:editarcliente.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    public function checkoutAction()
    {
		$carrito = $this->sessionManager->getSession('carrito');
		if(count($carrito)==0){
			return $this->redirect($this->generateUrl('_index'));
		}
		$productos = array();
		$em = $this->getDoctrine()->getManager();
		foreach ($carrito as $key => $slug) {
			$producto = $em->getRepository('GESTIONGestionBundle:Elemento')->findOneBy(array('slug'=>$slug));
			if($producto){
				$productos[] = $producto;
			}
		}
        return $this->render('GESTIONTiendaBundle:Default:checkout.html.twig', array(
							'title'=>'Molate Home - Check Out',
							'productos'=>$productos
						));
	}

    public function agregarAction(Request $request)
    {
		$this->sessionManager->agregar($request->get('slug'));
		return $this->redirect($this->generateUrl('_producto', array('slug'=>$request->get('slug'))));
	}

    public function quitarAction(Request $request)
    {
		$this->sessionManager->quitar($request->get('slug'));
		return $this->redirect($this->generateUrl('_checkout'));
	}

    public function vaciarAction(Request $request)
    {
		$this->sessionManager->vaciar();
		return $this->redirect($this->generateUrl('_index'));
	}

    public function realizarpedidoAction(Request $request)
    {
		$carrito = $this->sessionManager->getSession('carrito');
		if(count($carrito)==0){
			return $this->redirect($this->generateUrl('_index'));
		}

		$em = $this->getDoctrine()->getManager();
		$pedido = new Colaboracion();
		$total = 0;
		$cliente = $em->getRepository('GESTIONGestionBundle:Cliente')->find($this->sessionManager->getSession('cliente')->getId());
		$pedido->setCliente($cliente);
		$pedido->setDescripcion("Pedido desde la Tienda");
		$pedido->setTipo("TIENDA");

		foreach ($carrito as $slug) {
			$producto = $em->getRepository('GESTIONGestionBundle:Elemento')->findOneBy(array('slug'=>$slug));
			if($producto){
				$elementostocks = $em->getRepository('GESTIONGestionBundle:ElementoStock')->findBy(array('elemento'=>$producto->getId()), array('fecha'=>'ASC'));
				$cantidad = $request->get("cantidad$slug");

				$total+= $cantidad * $producto->getPrecio();
				foreach($elementostocks as $elementostock){
					if($cantidad > 0 and ($cantidad <= $elementostock->getStock() or $producto->getStock()==0) ){
						$elementocolaboracion = new ElementoColaboracion();
						$elementocolaboracion->setElementostock($elementostock);
						$elementocolaboracion->setColaboracion($pedido);
						$elementocolaboracion->setCantidad($cantidad);
						$elementocolaboracion->setPrecio($producto->getPrecio());
						$em->persist($elementocolaboracion);
						$cantidad = 0;
					}else{
						if($cantidad > 0 and $elementostock->getStock()>0){
							$elementocolaboracion = new ElementoColaboracion();
							$elementocolaboracion->setElementostock($elementostock);
							$elementocolaboracion->setColaboracion($pedido);
							$elementocolaboracion->setPrecio($producto->getPrecio());
							$elementocolaboracion->setCantidad($elementostock->getStock());
							$cantidad = $cantidad - $elementostock->getStock();

							$em->persist($elementocolaboracion);
						}
					}
				}
				if(count($elementostocks)==0){
					$elementostock = new ElementoStock();
					$elementostock->setElemento($producto);
					$elementostock->setCosto($producto->getPrecio());
					$elementostock->setTipofactura('N/A');
					$elementostock->setFactura('');
					$elementostock->setStock(0);

					$em->persist($elementostock);

					$elementocolaboracion = new ElementoColaboracion();
					$elementocolaboracion->setElementostock($elementostock);
					$elementocolaboracion->setColaboracion($pedido);
					$elementocolaboracion->setPrecio($producto->getPrecio());
					$elementocolaboracion->setCantidad($cantidad);
					$em->persist($elementocolaboracion);
				}
			}
		}

		$pedido->setTotal($total);
		$pedido->setCodEstado('P');
		$em->persist($pedido);

		$em->flush();

		$this->sessionManager->vaciar();
		$this->enviarMailPedido($pedido);
		$this->informarPedido($pedido);

		return $this->redirect($this->generateUrl('_elegirmedio', array('id'=> $pedido->getId()) ));

	}

	public function enviarMailPedido($pedido){
		$message = \Swift_Message::newInstance();
		$message->setContentType('text/html')
		->setSubject('MOLATE HOME - Pedido Realizado')
		->setFrom('info@molatehome.com.ar')
		->setTo($pedido->getCliente()->getMail())
		->setBody($this->renderView('GESTIONTiendaBundle:Default:mailnuevopedido.html.twig', array('pedido' => $pedido)));

		$this->get('mailer')->send($message);
	}

	public function mailAction(){

		$message = \Swift_Message::newInstance();
		$message->setContentType('text/html')
		->setSubject('MOLATE HOME - Pedido Realizado')
		->setFrom('info@molatehome.com.ar')
		->setTo(array("info@molatehome.com.ar", "corvalan4@gmail.com"))
		->setBody("Mail!");

		$this->get('mailer')->send($message);
		return new Response('Enviado?');
	}

	public function informarPedido($pedido){
		$message = \Swift_Message::newInstance();
		$message->setContentType('text/html')
		->setSubject('MOLATE HOME - Nuevo Pedido')
		->setFrom('info@molatehome.com.ar')
		->setTo('info@molatehome.com.ar')
		->setBody($this->renderView('GESTIONTiendaBundle:Default:informarnuevopedido.html.twig', array('pedido' => $pedido)));

		$this->get('mailer')->send($message);
	}

	public function elegirmedioAction($id){
		$em = $this->getDoctrine()->getManager();

		$pedido = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

		if(!$pedido){
			return $this->redirect($this->generateUrl('_index'));
		}

		$linkpago = $this->sessionManager->generarMercadoPago($pedido);

		return $this->render('GESTIONTiendaBundle:Default:elegirmedio.html.twig', array( 'title'=>'Molate Home - Elegir Medio', 'linkpago'=>$linkpago, 'pedido'=>$pedido));
	}

    public function respuestapagoAction($id, $estado)
    {
		$em = $this->getDoctrine()->getManager();
		$pedido = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);
		if($pedido){
			if($estado == 'success'){
				$pedido->setCodEstado('PNE');
				$pedido->setDescripcion('Pagado mediante MP - '.$pedido->getDescripcion());
			}
			if($estado == 'failure'){
				$pedido->setDescripcion('Fallo intento de pago por MP - '.$pedido->getDescripcion());
			}
			if($estado == 'pending'){
				$pedido->setDescripcion('Pendiente de pago - '.$pedido->getDescripcion());
			}
			$em->flush();
		}

		return $this->redirect($this->generateUrl('_index'));

	}

    public function mispedidosAction()
    {
		$em = $this->getDoctrine()->getManager();

		$cliente = $this->sessionManager->getSession('cliente');

		if(!$cliente){
			return $this->redirect($this->generateUrl('_index'));
		}

		$pedidos = $em->getRepository('GESTIONGestionBundle:Colaboracion')->findBy(array('cliente'=>$cliente->getId()), array('id'=>'DESC'));

		return $this->render('GESTIONTiendaBundle:Default:mispedidos.html.twig', array( 'title'=>'Molate Home - Mis Pedidos', 'pedidos'=>$pedidos));
	}

    public function verpedidoAction($id)
    {
		$em = $this->getDoctrine()->getManager();

		$pedido = $em->getRepository('GESTIONGestionBundle:Colaboracion')->find($id);

		if(!$pedido){
			return $this->redirect($this->generateUrl('_index'));
		}

		return $this->render('GESTIONTiendaBundle:Default:verpedido.html.twig', array( 'title'=>'Molate Home - Pedido', 'pedido'=>$pedido));
	}
}
?>
