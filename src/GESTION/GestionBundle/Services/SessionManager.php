<?php

namespace GESTION\GestionBundle\Services;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use GESTION\GestionBundle\Entity\MP;

class SessionManager
{
    /**
     *
     * @var Container
     */
    public $container;

    /**
     * @var EntityManager
     */
    public $em;

    /**
     * @var Session
     */
    public $session;

    public function __construct(Container $container){
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->session = $container->get('session');
    }

    public function isStarted() {
		return !empty($this->getSession('categorias')) ? true : false;
	}

    public function limpiar() {
        $categorias = $this->em->getRepository('GESTIONGestionBundle:Categoria')->findBy(array('cod_estado'=>'A'), array('nombre'=>'ASC'));
		$this->setSession('categorias', $categorias);
	}

    public function startSession() {
        $this->session->start();
		$this->setSession('perfil', '');
		$this->setSession('login', false);
		$this->setSession('usuario', '');
		$this->setSession('idUsuario', '');
        $categorias = $this->em->getRepository('GESTIONGestionBundle:Categoria')->findBy(array('cod_estado'=>'A'), array('nombre'=>'ASC'));
		$this->setSession('categorias', $categorias);
    }

    public function clearSession(){
        $this->session->clear();
        $this->session->getFlashBag()->clear();
    }

    public function closeSession(){
        $this->clearSession();
        $this->startSession();
    }

	/**
    * Agrega un mensaje de tipo msgType a la siguiente response.
    * msgType validos: msgOk, msgInfo, msgWarn, msgError.
    * @param string $msgType
    * @param string $msg
    */
    public function addFlash($msgType, $msg){
        $this->session->getFlashBag()->add($msgType, $msg);
    }

    /**
     * Setea un parametro en la sesion
     * @param string $attr
     * @param string $value
     * @return mixed
     */
    public function setSession($attr, $value){
        $this->session->set($attr, $value);
    }

   /**
     * Devuelve un valor de la sesion
     * @param string $attr
     * @return mixed
     */
    public function getSession($attr){
        return $this->session->get($attr);
    }

	public function login($login, $pass){
		$usuario = $this->em->getRepository('GESTIONGestionBundle:Usuario')->findOneBy(array('login' => $login, 'clave'=>$pass));
		if ($usuario != null){
			$this->setSession('login', true);
			$this->setSession('perfil', $usuario->getPerfil());
			$this->setSession('idUsuario', $usuario->getId());
			$this->setSession('usuario', $login);
			return true;
        }else{
			$this->addFlash('msgError','El usuario no existe.');
			return false;
		}
	}

    public function isLogged(){
        return !empty($this->getSession('login')) ? $this->getSession('login') : false;
    }

	public function decryptIt( $q ) {
		$cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
		$qDecoded  = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
		return( $qDecoded );
	}

    public function encryptIt( $q ) {
    	$cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
    	$qEncoded = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
    	return( $qEncoded );
    }
	public function getSlug($text){
		return strtolower(str_replace(" ", "-", $this->eliminaAcentos($text)));
	}

    public function agregar($slug){
        if(empty($slug)){
            $this->addFlash('msgError', 'Es necesario seleccionar un producto.');
        }else{
            $productosCarrito = (!empty($this->getSession('carrito'))) ? $this->getSession('carrito') : array();
            if(!in_array($slug, $productosCarrito)){
                array_push($productosCarrito, $slug);
            }
            $this->setSession('carrito', $productosCarrito);
            $this->addFlash('msgOk', 'El producto fue AGREGADO al carrito.');
        }
	}

    public function quitar($slug){
        $productosCarrito = (!empty($this->getSession('carrito'))) ? $this->getSession('carrito') : array();
        $productosCarrito = array_diff($productosCarrito, array($slug));
        $this->setSession('carrito', $productosCarrito);

        $this->addFlash('msgOk', 'El producto fue QUITADO al carrito.');
	}

    public function vaciar(){
        $this->setSession('carrito', array());

        $this->addFlash('msgOk', 'El carrito ha sido vaciado.');
	}

	public function eliminaAcentos($text)
    {
        $text = htmlentities($text, ENT_QUOTES, 'UTF-8');
        $text = strtolower($text);
        $patron = array (
            // Espacios, puntos y comas por guion
            //'/[\., ]+/' => ' ',

            // Vocales
            '/\+/' => '',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',

            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',

            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',

            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',

            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',

            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',

            // Otras letras y caracteres especiales
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'n',

            // Agregar aqui mas caracteres si es necesario

        );

        $text = preg_replace(array_keys($patron),array_values($patron),$text);
        return $text;
    }

    public function generarMercadoPago($pedido){
    		// Claves otorogadas por MP
    		$mp = new MP("50778135626636", "zi6W1vPBE77h65CoiFTQXEeh9S1jX05T");

    		// Modo SANDBOX
    		// $mp->sandbox_mode(FALSE);
    		$mp->sandbox_mode(TRUE);

    		// Armamos un array de Items, para reutilizar el codigo con mas de un producto.
    		$items = array();
            foreach($pedido->getElementoscolaboracion() as $elementocolaboracion){
        		$item = array("title" => $elementocolaboracion->getElementostock()->getElemento()->getNombre(),
                              "quantity" => $elementocolaboracion->getCantidad(),
                              "currency_id" => "ARS",
                              "unit_price" => $elementocolaboracion->getPrecio());
        		array_push($items, $item);
            }

    		// URLs de retorno a nuestro sistema.
    		$back = array(
    			"success" => 'http://localhost/MOLATE/web/app_dev.php/respuestapago/'.$pedido->getId().'/success',
    			"failure" => 'http://localhost/MOLATE/web/app_dev.php/respuestapago/'.$pedido->getId().'/failure',
    			"pending" => 'http://localhost/MOLATE/web/app_dev.php/respuestapago/'.$pedido->getId().'/pending'
            );

/*
    		$back = array(
    			"success" => 'http://molatehome.com.ar/respuestapago/'.$pedido->getId().'/success',
    			"failure" => 'http://molatehome.com.ar/respuestapago/'.$pedido->getId().'/failure',
    			"pending" => 'http://molatehome.com.ar/respuestapago/'.$pedido->getId().'/pending'
            );
*/

    		// Armado del Boton
    		$preference_data = array(
    			"items" => $items,
    			"back_urls"=>$back,
    			"external_reference"=>$pedido->getId(),
    		);

    		$preference = $mp->create_preference($preference_data);

            // return $preference['response']['init_point'];
            return $preference['response']['sandbox_init_point'];
    }
}
