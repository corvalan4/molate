<?php
namespace GESTION\GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="GESTION\GestionBundle\Entity\ElementoRepository")
 * @ORM\Table(name="elemento")
 */
class Elemento{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $codigo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $novedad = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $visible = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $destacado = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $stockminimo = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $precio = 0;

    /**
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    protected $imagen;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    protected $imagendos;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */
    protected $imagentres;

    /**
     * @ORM\OneToMany(targetEntity="ElementoStock", mappedBy="elemento")
     */
    protected $stocks;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="elementos")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id")
     */
    protected $categoria;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $cod_estado = 'A';

    /**********************************
     * __construct
     *
     *
     **********************************/
	public function __construct()
	{
		$this->stocks = new ArrayCollection();
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/
	 public function __toString()
	{
		return $this->getCodigo();
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Elemento
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Elemento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
		$stocktotal = 0;
		foreach($this->stocks as $stock){
			$stocktotal+=$stock->getStock();
		}
        return $stocktotal;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Elemento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Elemento
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    public function getImagenBlob()
    {
		if(!empty($this->imagen)){
			return stream_get_contents( $this->imagen );
		}else{
			return "";
		}
    }

    /**
     * Set imagendos
     *
     * @param string $imagendos
     * @return Elemento
     */
    public function setImagendos($imagendos)
    {
        $this->imagendos = $imagendos;

        return $this;
    }

    /**
     * Get imagendos
     *
     * @return string
     */
    public function getImagendos()
    {
        return $this->imagendos;
    }

    public function getImagendosBlob()
    {
		if(!empty($this->imagendos)){
			return stream_get_contents( $this->imagendos );
		}else{
			return "";
		}
    }

    /**
     * Set imagentres
     *
     * @param string $imagentres
     * @return Elemento
     */
    public function setImagentres($imagentres)
    {
        $this->imagentres = $imagentres;

        return $this;
    }

    /**
     * Get imagentres
     *
     * @return string
     */
    public function getImagentres()
    {
        return $this->imagentres;
    }

    public function getImagentresBlob()
    {
		if(!empty($this->imagentres)){
			return stream_get_contents( $this->imagentres );
		}else{
			return "";
		}
    }

    /**
     * Set cod_estado
     *
     * @param string $codEstado
     * @return Elemento
     */
    public function setCodEstado($codEstado)
    {
        $this->cod_estado = $codEstado;

        return $this;
    }

    /**
     * Get cod_estado
     *
     * @return string
     */
    public function getCodEstado()
    {
        return $this->cod_estado;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Elemento
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set categoria
     *
     * @param \GESTION\GestionBundle\Entity\Categoria $categoria
     * @return Elemento
     */
    public function setCategoria(\GESTION\GestionBundle\Entity\Categoria $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \GESTION\GestionBundle\Entity\Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set stockminimo
     *
     * @param integer $stockminimo
     * @return Elemento
     */
    public function setStockminimo($stockminimo)
    {
        $this->stockminimo = $stockminimo;

        return $this;
    }

    /**
     * Get stockminimo
     *
     * @return integer
     */
    public function getStockminimo()
    {
        return $this->stockminimo;
    }

    /**
     * Set precio
     *
     * @param float $precio
     * @return Elemento
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set dependenciaorigen
     *
     * @param string $dependenciaorigen
     * @return Categoria
     */
    public function setDependenciaorigen($dependenciaorigen)
    {
        $this->dependenciaorigen = $dependenciaorigen;

        return $this;
    }

    /**
     * Get dependenciaorigen
     *
     * @return string
     */
    public function getDependenciaorigen()
    {
        return $this->dependenciaorigen;
    }

    /**
     * Add stocks
     *
     * @param \GESTION\GestionBundle\Entity\ElementoStock $stocks
     * @return Elemento
     */
    public function addStock(\GESTION\GestionBundle\Entity\ElementoStock $stocks)
    {
        $this->stocks[] = $stocks;

        return $this;
    }

    /**
     * Remove stocks
     *
     * @param \GESTION\GestionBundle\Entity\ElementoStock $stocks
     */
    public function removeStock(\GESTION\GestionBundle\Entity\ElementoStock $stocks)
    {
        $this->stocks->removeElement($stocks);
    }

    /**
     * Get stocks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStocks()
    {
        return $this->stocks;
    }

    /**
     * Set novedad
     *
     * @param boolean $novedad
     * @return Elemento
     */
    public function setNovedad($novedad)
    {
        $this->novedad = $novedad;

        return $this;
    }

    /**
     * Get novedad
     *
     * @return boolean
     */
    public function getNovedad()
    {
        return $this->novedad;
    }

    /**
     * Set destacado
     *
     * @param boolean $destacado
     * @return Elemento
     */
    public function setDestacado($destacado)
    {
        $this->destacado = $destacado;

        return $this;
    }

    /**
     * Get destacado
     *
     * @return boolean
     */
    public function getDestacado()
    {
        return $this->destacado;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Elemento
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }
}
