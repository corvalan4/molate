<?php
namespace GESTION\GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="GESTION\GestionBundle\Entity\ColaboracionRepository")
 * @ORM\Table(name="factura")
 */
class Colaboracion{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Cliente", inversedBy="colaboraciones")
     * @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    protected $cliente;

    /**
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $tipo="SISTEMA";

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $descuento = 0;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $bonificacion = 0;

    /**
     * @ORM\OneToMany(targetEntity="Adjunto", mappedBy="colaboracion")
     */
    protected $adjuntos;

    /**
     * @ORM\OneToMany(targetEntity="ElementoColaboracion", mappedBy="colaboracion")
     */
    protected $elementoscolaboracion;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $cod_estado = 'A';

    /**********************************
     * __construct
     *
     *
     **********************************/
	public function __construct()
	{
		$this->elementoscolaboracion = new ArrayCollection();
		$this->adjuntos = new ArrayCollection();
		$this->fecha = new \DateTime('NOW');
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/
	 public function __toString()
	{
		return "ENT:".str_pad($this->id, 5, "0", STR_PAD_LEFT);;
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Colaboracion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Colaboracion
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set total
     *
     * @param float $total
     * @return Colaboracion
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set cod_estado
     *
     * @param string $codEstado
     * @return Colaboracion
     */
    public function setCodEstado($codEstado)
    {
        $this->cod_estado = $codEstado;

        return $this;
    }

    /**
     * Get cod_estado
     *
     * @return string
     */
    public function getCodEstado()
    {
        return $this->cod_estado;
    }

    public function getEstadoStr()
    {
        if($this->cod_estado=='PE'){
            return "<strong style='color:green;'>PAGADO Y ENTREGADO</strong>";
        }
        if($this->cod_estado=='PNE'){
            return "<strong style='color:#1307ca;'>PAGADO Y NO ENTREGADO</strong>";
        }
        if($this->cod_estado=='P'){
            return "<strong style='color: orange;'>EN PROCESO</strong>";
        }
        if($this->cod_estado=='E' or $this->cod_estado=='C'){
            return "<strong style='color: red;'>CANCELADO</strong>";
        }
        if($this->cod_estado=='A'){
            return "<strong>ENTREGADO</strong>";
        }
    }

    /**
     * Add adjuntos
     *
     * @param \GESTION\GestionBundle\Entity\Adjunto $adjuntos
     * @return Colaboracion
     */
    public function addAdjunto(\GESTION\GestionBundle\Entity\Adjunto $adjuntos)
    {
        $this->adjuntos[] = $adjuntos;

        return $this;
    }

    /**
     * Remove adjuntos
     *
     * @param \GESTION\GestionBundle\Entity\Adjunto $adjuntos
     */
    public function removeAdjunto(\GESTION\GestionBundle\Entity\Adjunto $adjuntos)
    {
        $this->adjuntos->removeElement($adjuntos);
    }

    /**
     * Get adjuntos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdjuntos()
    {
        return $this->adjuntos;
    }

    /**
     * Add elementoscolaboracion
     *
     * @param \GESTION\GestionBundle\Entity\ElementoColaboracion $elementoscolaboracion
     * @return Colaboracion
     */
    public function addElementoscolaboracion(\GESTION\GestionBundle\Entity\ElementoColaboracion $elementoscolaboracion)
    {
        $this->elementoscolaboracion[] = $elementoscolaboracion;

        return $this;
    }

    /**
     * Remove elementoscolaboracion
     *
     * @param \GESTION\GestionBundle\Entity\ElementoColaboracion $elementoscolaboracion
     */
    public function removeElementoscolaboracion(\GESTION\GestionBundle\Entity\ElementoColaboracion $elementoscolaboracion)
    {
        $this->elementoscolaboracion->removeElement($elementoscolaboracion);
    }

    /**
     * Get elementoscolaboracion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getElementoscolaboracion()
    {
        return $this->elementoscolaboracion;
    }

    /**
     * Set cliente
     *
     * @param \GESTION\GestionBundle\Entity\Cliente $cliente
     * @return Colaboracion
     */
    public function setCliente(\GESTION\GestionBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \GESTION\GestionBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set descuento
     *
     * @param float $descuento
     * @return Colaboracion
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;

        return $this;
    }

    /**
     * Get descuento
     *
     * @return float
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    /**
     * Set bonificacion
     *
     * @param float $bonificacion
     * @return Colaboracion
     */
    public function setBonificacion($bonificacion)
    {
        $this->bonificacion = $bonificacion;

        return $this;
    }

    /**
     * Get bonificacion
     *
     * @return float
     */
    public function getBonificacion()
    {
        return $this->bonificacion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Colaboracion
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
