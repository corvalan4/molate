<?php
namespace GESTION\GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="GESTION\GestionBundle\Entity\DocumentoRepository")
 * @ORM\Table(name="documento")
 */
class Documento{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
		
	/**
	* @ORM\ManyToOne(targetEntity="TipoGasto", inversedBy="documentos")
	* @ORM\JoinColumn(name="tipoGasto_id", referencedColumnName="id", nullable=true)
	*/
	protected $tipoGasto;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    protected $importe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $descripcion;

    /**
     * @ORM\Column(type="string", length=2, nullable=false)
     */
    protected $tipoDocumento = 'G';

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
	}
		

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	public function __toString()
	{
		if($this->tipoDocumento == 'G'){
			return "G".str_pad($this->id, 6, '0', STR_PAD_LEFT);
		}
	}		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Documento
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set importe
     *
     * @param string $importe
     * @return Documento
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    
        return $this;
    }

    /**
     * Get importe
     *
     * @return string 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Documento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipoDocumento
     *
     * @param string $tipoDocumento
     * @return Documento
     */
    public function setTipoDocumento($tipoDocumento)
    {
        $this->tipoDocumento = $tipoDocumento;
    
        return $this;
    }

    /**
     * Get tipoDocumento
     *
     * @return string 
     */
    public function getTipoDocumento()
    {
        return $this->tipoDocumento;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Documento
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set tipoGasto
     *
     * @param \GESTION\GestionBundle\Entity\TipoGasto $tipoGasto
     * @return Documento
     */
    public function setTipoGasto(\GESTION\GestionBundle\Entity\TipoGasto $tipoGasto = null)
    {
        $this->tipoGasto = $tipoGasto;
    
        return $this;
    }

    /**
     * Get tipoGasto
     *
     * @return \GESTION\GestionBundle\Entity\TipoGasto 
     */
    public function getTipoGasto()
    {
        return $this->tipoGasto;
    }
}