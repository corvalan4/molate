<?php
namespace GESTION\GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="GESTION\GestionBundle\Entity\CategoriaRepository")
 * @ORM\Table(name="categoria")
 */
class Categoria{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $slug;
	
    /**
     * @ORM\OneToMany(targetEntity="Elemento", mappedBy="categoria")
     */
    protected $elementos;
	 
	 
    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $cod_estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
		$this->elementos = new ArrayCollection();
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	 public function __toString()
	{
		return $this->getNombre();
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Elemento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
	
    /**
     * Set slug
     *
     * @param string $slug
     * @return Elemento
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set cod_estado
     *
     * @param string $codEstado
     * @return Elemento
     */
    public function setCodEstado($codEstado)
    {
        $this->cod_estado = $codEstado;
    
        return $this;
    }

    /**
     * Get cod_estado
     *
     * @return string 
     */
    public function getCodEstado()
    {
        return $this->cod_estado;
    }

    /**
     * Add elementos
     *
     * @param \GESTION\GestionBundle\Entity\Elemento $elementos
     * @return Categoria
     */
    public function addElemento(\GESTION\GestionBundle\Entity\Elemento $elementos)
    {
        $this->elementos[] = $elementos;
    
        return $this;
    }

    /**
     * Remove elementos
     *
     * @param \GESTION\GestionBundle\Entity\Elemento $elementos
     */
    public function removeElemento(\GESTION\GestionBundle\Entity\Elemento $elementos)
    {
        $this->elementos->removeElement($elementos);
    }

    /**
     * Get elementos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getElementos()
    {
        return $this->elementos;
    }
}