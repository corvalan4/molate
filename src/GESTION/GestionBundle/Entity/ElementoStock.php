<?php
namespace GESTION\GestionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="GESTION\GestionBundle\Entity\ElementoStockRepository")
 * @ORM\Table(name="elementostock")
 */
class ElementoStock{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $tipofactura;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $factura;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fecha;

    /**
     * @ORM\OneToMany(targetEntity="ElementoColaboracion", mappedBy="elementostock")
     */
    protected $elementoscolaboracion;

    /**
     * @ORM\ManyToOne(targetEntity="Elemento", inversedBy="stocks")
     * @ORM\JoinColumn(name="elemento_id", referencedColumnName="id")
     */
    protected $elemento;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $stock;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $costo;

    /**
     * @ORM\Column(type="string", length=1)
     */
    protected $cod_estado = 'A';

    /**********************************
     * __construct
     *
     * 
     **********************************/        
	public function __construct()
	{
		$this->elementoscolaboracion = new ArrayCollection();
		$this->fecha = new \DateTime('NOW');
	}

	/**********************************
     * __toString()
     *
     * Este método sirve para poder popular los comboboxes en los forms.
     *********************************/ 
	public function __toString()
	{
		return $this->getStock();
	}		

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set origen
     *
     * @param string $origen
     * @return ElementoStock
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;
    
        return $this;
    }

    /**
     * Get origen
     *
     * @return string 
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return ElementoStock
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    
        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set ordencompra
     *
     * @param string $ordencompra
     * @return ElementoStock
     */
    public function setOrdencompra($ordencompra)
    {
        $this->ordencompra = $ordencompra;
    
        return $this;
    }

    /**
     * Get ordencompra
     *
     * @return string 
     */
    public function getOrdencompra()
    {
        return $this->ordencompra;
    }

    /**
     * Set tipofactura
     *
     * @param string $tipofactura
     * @return ElementoStock
     */
    public function setTipofactura($tipofactura)
    {
        $this->tipofactura = $tipofactura;
    
        return $this;
    }

    /**
     * Get tipofactura
     *
     * @return string 
     */
    public function getTipofactura()
    {
        return $this->tipofactura;
    }

    /**
     * Set factura
     *
     * @param string $factura
     * @return ElementoStock
     */
    public function setFactura($factura)
    {
        $this->factura = $factura;
    
        return $this;
    }

    /**
     * Get factura
     *
     * @return string 
     */
    public function getFactura()
    {
        return $this->factura;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return ElementoStock
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return ElementoStock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    
        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStockUsado()
    {
		$stockusado = 0;
		foreach($this->elementoscolaboracion as $elementocolaboracion){
			if($elementocolaboracion->getColaboracion()->getCodEstado() == 'A'){
				$stockusado += $elementocolaboracion->getCantidad(); 
			}
		}
        return $stockusado;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set stockhistorico
     *
     * @param integer $stockhistorico
     * @return ElementoStock
     */
    public function setStockhistorico($stockhistorico)
    {
        $this->stockhistorico = $stockhistorico;
    
        return $this;
    }

    /**
     * Get stockstockhistorico
     *
     * @return integer 
     */
    public function getStockhistorico()
    {
        return $this->stockstockhistorico;
    }

    /**
     * Set cod_estado
     *
     * @param string $codEstado
     * @return ElementoStock
     */
    public function setCodEstado($codEstado)
    {
        $this->cod_estado = $codEstado;
    
        return $this;
    }

    /**
     * Get cod_estado
     *
     * @return string 
     */
    public function getCodEstado()
    {
        return $this->cod_estado;
    }

    /**
     * Set elemento
     *
     * @param \GESTION\GestionBundle\Entity\Elemento $elemento
     * @return ElementoStock
     */
    public function setElemento(\GESTION\GestionBundle\Entity\Elemento $elemento = null)
    {
        $this->elemento = $elemento;
    
        return $this;
    }

    /**
     * Get elemento
     *
     * @return \GESTION\GestionBundle\Entity\Elemento 
     */
    public function getElemento()
    {
        return $this->elemento;
    }

    /**
     * Add elementoscolaboracion
     *
     * @param \GESTION\GestionBundle\Entity\ElementoColaboracion $elementoscolaboracion
     * @return Elemento
     */
    public function addElementoscolaboracion(\GESTION\GestionBundle\Entity\ElementoColaboracion $elementoscolaboracion)
    {
        $this->elementoscolaboracion[] = $elementoscolaboracion;
    
        return $this;
    }

    /**
     * Remove elementoscolaboracion
     *
     * @param \GESTION\GestionBundle\Entity\ElementoColaboracion $elementoscolaboracion
     */
    public function removeElementoscolaboracion(\GESTION\GestionBundle\Entity\ElementoColaboracion $elementoscolaboracion)
    {
        $this->elementoscolaboracion->removeElement($elementoscolaboracion);
    }

    /**
     * Get elementoscolaboracion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getElementoscolaboracion()
    {
        return $this->elementoscolaboracion;
    }

    /**
     * Set costo
     *
     * @param float $costo
     * @return ElementoStock
     */
    public function setCosto($costo)
    {
        $this->costo = $costo;
    
        return $this;
    }

    /**
     * Get costo
     *
     * @return float 
     */
    public function getCosto()
    {
        return $this->costo;
    }
}