<?php

namespace GESTION\GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;


class ElementoType extends AbstractType
{
	
	/**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
	private $value;
	public function __construct($iddep){
		$this->value = $iddep;
	}

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$iddep = $this->value;
        $builder
            ->add('nombre')
            ->add('codigo')
			->add('categoria', 'entity', array (
				'class' => 'GESTIONGestionBundle:Categoria',
				'label' => 'Categoria',
				'query_builder' => function (\GESTION\GestionBundle\Entity\CategoriaRepository $repository)  use ( $iddep )
					 {
						 return $repository->createQueryBuilder('u')->where('u.cod_estado = :ACTIVO')->orderBy('u.nombre', 'asc')->setParameter(':ACTIVO', 'A');											 					 	 }
					))
            ->add('stockminimo', 'text', array('label'=>'Stock Mínimo'))
            ->add('novedad')
            ->add('destacado')
            ->add('precio', 'text', array('label'=>'Precio'))
            ->add('descripcion', 'textarea', array('attr'=>array('class'=>'form-control')))
            ->add('imagen', 'file', array('label'=>'Imagen Principal', 'mapped'=>false, 'attr'=>array('accept'=>'image/*')))
            ->add('imagendos', 'file', array('label'=>'Imagen dos', 'mapped'=>false, 'attr'=>array('accept'=>'image/*')))
            ->add('imagentres', 'file', array('label'=>'Imagen tres', 'mapped'=>false, 'attr'=>array('accept'=>'image/*')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GESTION\GestionBundle\Entity\Elemento'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gestion_gestionbundle_elemento';
    }
}
