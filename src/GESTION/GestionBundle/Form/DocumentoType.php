<?php

namespace GESTION\GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DocumentoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
			$builder
				->add('fecha')
				->add('tipogasto', 'entity', array (
					'class' => 'GESTIONGestionBundle:TipoGasto',
					'label' => 'Tipo de Gasto',
					'query_builder' => function (\GESTION\GestionBundle\Entity\TipoGastoRepository $repository)
						{
							 return $repository->createQueryBuilder('u')->where('u.estado = ?1')->setParameter(1, 'A')->orderBy('u.nombre', 'ASC');
						}
						))
				->add('importe')
                ->add('descripcion', 'textarea', array ('label' => 'Descripcion', 'attr'=> array('class'=>'form-control', 'style'=>'height:200px')))
                ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GESTION\GestionBundle\Entity\Documento'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bundle_documento';
    }
}
