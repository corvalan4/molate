<?php

namespace GESTION\GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ColaboracionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cliente', 'text', array(
				'mapped'=>false, 
				'label'=>'Cliente'
			))
            ->add('idcliente', 'text', array(
				'mapped'=>false, 
				'label'=>'ID - Cliente'
			))            
            ->add('archivo', 'file', array(
				'mapped'=>false, 
				'label'=>'Archivo'
			))
            ->add('descripcion', 'textarea', array(
				'label'=>'Descripción',
				'attr'=>array('class'=>'form-control', 'attr'=>'height: 200px;')
			))
            ->add('descuento', 'text', array(
				'label'=>'Descuento'
			))
            ->add('bonificacion', 'text', array(
				'label'=>'Bonificacion'
			))
            ->add('fecha', 'text', array(
				'mapped'=>false, 
				'label'=>'Fecha',
				'attr'=>array('class'=>'datetimepicker', 'value'=>date('Y-m-d'))
			))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GESTION\GestionBundle\Entity\Colaboracion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gestion_gestionbundle_colaboracion';
    }
}
