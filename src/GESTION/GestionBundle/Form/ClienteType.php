<?php

namespace GESTION\GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClienteType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('razonSocial', 'text', array('label' => $options['razonSocialLabel']))
            ->add('cuit', 'text', array('label' => 'CUIT'))
            ->add('localidad', 'text', array('label' => 'Localidad'))
            ->add('direccion', 'text', array('label' => $options['direccionLabel']))
            ->add('telefono', 'text', array('label' => $options['telefonoLabel']))
            ->add('mail')            
            ->add('clave')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GESTION\GestionBundle\Entity\Cliente',
			'razonSocialLabel' =>  'Razón Social',
			'direccionLabel' =>  'Dirección',
			'telefonoLabel' =>  'Teléfono'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bundle_cliente';
    }
}
