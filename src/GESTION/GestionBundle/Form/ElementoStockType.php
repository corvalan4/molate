<?php

namespace GESTION\GestionBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ElementoStockType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tipofactura', 'choice', array(
				'choices'  => array(
					'A' => 'A',
					'B' => 'B',
					'C' => 'C',
					'PV' => 'PV',
					'N/A' => 'N/A',
					'CM' => 'CM',
				),
			))
            ->add('elemento')
            ->add('factura', 'text', array('label'=>'N° de Factura'))
            ->add('stock', 'integer', array('label'=>'Stock', 'attr'=>array('min'=>1)))
            ->add('costo', 'text', array('label'=>'Costo'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GESTION\GestionBundle\Entity\ElementoStock'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gestion_gestionbundle_elementostock';
    }
}
