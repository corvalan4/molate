<?php

namespace GESTION\GestionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use GESTION\GestionBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;
use Doctrine\Common\Collections\ArrayCollection;
use GESTION\GestionBundle\Entity\Elemento;
use GESTION\GestionBundle\Entity\ElementoStock;

class DefaultController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;
	
    public function indexAction(Request $request)
    {		
		$this->sessionManager->startSession();
        return $this->render('GESTIONGestionBundle:Default:index.html.twig');
	}
	
    public function inicioAction()
    {
		if(!empty($this->sessionManager->getSession('login')) and $this->sessionManager->getSession('login')==true){
			return $this->render('GESTIONGestionBundle:Default:inicio.html.twig');
		}
			
		$this->sessionManager->startSession();
		return $this->redirect($this->generateUrl('_homepage'));
	}
	
    public function cerrarAction()
    {
		$this->sessionManager->closeSession();
		return $this->redirect($this->generateUrl('_homepage'));
	}
	
    public function loginAction(Request $request)
    {
		if($this->sessionManager->login($request->get('usuario'), $request->get('clave'))){
            return $this->redirect($this->generateUrl('_inicio'));
		}else{
            return $this->redirect($this->generateUrl('_homepage'));
		}
	}
	
    public function informeingresosegresosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

		$desde = $request->get('desde');
		$hasta = $request->get('hasta');

		$ingresos = new ArrayCollection();
		$egresos = new ArrayCollection();

		if(!empty($desde) and !empty($hasta)){
			$ingresos = $em->getRepository('GESTIONGestionBundle:Elemento')->ingresos(new \DateTime($desde), new \DateTime($hasta.' + 1 days'), $this->sessionManager->getSession('iddependencia'));
			$egresos = $em->getRepository('GESTIONGestionBundle:Elemento')->egresos(new \DateTime($desde), new \DateTime($hasta.' + 1 days'), $this->sessionManager->getSession('iddependencia'));
		}
        
		return $this->render('GESTIONGestionBundle:Default:informeingresosegresos.html.twig', array('ingresos'=>$ingresos, 'egresos'=>$egresos));
	}
	
    public function gananciasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
		
		if(empty($request->get('desde')) or empty($request->get('hasta'))){
			$desde = "";
			$hasta = "";
			$entities = new ArrayCollection();
			$gastos = new ArrayCollection();
			$arrRespuesta = array(
							'gastos' => $gastos,
							'entities' => $entities,
							'desde' => "",
							'hasta' => "",
							);
		}else{
			$desde = new \DateTime($request->get('desde'));
			$hasta = new \DateTime($request->get('hasta'));
			$entities = $em->getRepository('GESTIONGestionBundle:Colaboracion')->ganancia($desde, $hasta);
			$gastos = $em->getRepository('GESTIONGestionBundle:Documento')->filtro($desde, $hasta);
			$arrRespuesta = array(
							'gastos' => $gastos,
							'entities' => $entities,
							'desde' => $desde->format('Y-m-d'),
							'hasta' => $hasta->format('Y-m-d'),
							);
		}
				

        return $this->render('GESTIONGestionBundle:Default:ganancias.html.twig', $arrRespuesta);
    }
    
	public function gananciaventasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
		
		if(empty($request->get('desde')) or empty($request->get('hasta'))){
			$desde = "";
			$hasta = "";
			$entities = new ArrayCollection();
			$gastos = new ArrayCollection();
			$arrRespuesta = array(
							'gastos' => $gastos,
							'entities' => $entities,
							'desde' => "",
							'hasta' => "",
							);
		}else{
			$desde = new \DateTime($request->get('desde'));
			$hasta = new \DateTime($request->get('hasta'));
			$entities = $em->getRepository('GESTIONGestionBundle:Colaboracion')->gananciaventas($desde, $hasta);
			$gastos = $em->getRepository('GESTIONGestionBundle:Documento')->filtro($desde, $hasta);
			$arrRespuesta = array(
							'gastos' => $gastos,
							'entities' => $entities,
							'desde' => $desde->format('Y-m-d'),
							'hasta' => $hasta->format('Y-m-d'),
							);
		}
				

        return $this->render('GESTIONGestionBundle:Default:gananciaventas.html.twig', $arrRespuesta);
    }

    public function predictivaPropiaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
		
        $string = '<ul>';
		if(strlen($request->get('valor'))>2){
			$entities = $em->getRepository('GESTIONGestionBundle:Elemento')->predictiva($request->get('repositorio'), $request->get('campo'), $request->get('valor'));
			
			foreach ($entities as $entity) {
				if($request->get('repositorio')=='Elemento'){
					$string = $string . '<li class=suggest-element id=' . $entity->getId() . ' stock=' . $entity->getStock() . ' precio=' . $entity->getPrecio() . '><a href="#" id=predictivaelemento' . $entity->getId() . ' data=' . $entity->getId() . ' >' . $entity->getCodigo() . ' - '. $entity->getNombre();
				}else{
					$string = $string . '<li class=suggest-element id=' . $entity->getId() . '><a href="#" id=busqueda' . $entity->getId() . ' data=' . $entity->getId() . ' >' . $entity;
				}
			}
			$string = $string.'</ul>';
		}
		return new Response($string);
	}
	
    public function cargaMasivaAction()
    {
		$em = $this->getDoctrine()->getManager();

		return $this->render('GESTIONGestionBundle:Default:cargamasiva.html.twig');
	}

    public function importarArchivosAction(Request $request)
    {	
		$em = $this->getDoctrine()->getManager();
		$phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
		$uploaddir = $this->get('kernel')->getRootDir()."/../web/uploads/documents/";
		// Cell caching ::
        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod);
		$cA = 0;
		$filename = '';
		$a = $request->get('desde');
		$b = $a + 100;
		$existe = true;
		$hayAlgo = 'NO';

		if($a == 1){
			$name = trim($_FILES['archivo']['name']); 
			$filesize = $_FILES['archivo']['size'];
			$uploadfile = $uploaddir . $name;
			$files = $request->files;
			$uploadedFile = $files->get('archivo');

			if ($uploadedFile->move($uploaddir, $name)){
			}else{
				$this->sessionManager->addFlash('msgError', 'No se pudo cargar el archivo.');
			}
		}else{
			$uploadfile = $this->sessionManager->getSession('uploadfile');
		}

		if (!file_exists($uploadfile)) {
			$this->sessionManager->addFlash('msgError', 'No se pudo migrar el archivo.');
			return $this->redirect($this->generateUrl('_cargamasiva'));
		}
        
		ini_set('memory_limit', '-1');
		$inputFileType = 'Excel2007';
		$sheetname = 'Hoja1';
		$objReader =  \PHPExcel_IOFactory::createReader($inputFileType); 
		$objReader->setLoadSheetsOnly($sheetname); 
		$objPHPExcel = $objReader->load($uploadfile);
		$vacio = false;
	
		foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
			for($i = $a; $i<=$b; $i++) {				
				$rowIndex = $i;
				if (!$vacio && $rowIndex!=1 ){
					for($columnas=1;$columnas<11;$columnas++){
						if($columnas==1){$letracont='A';}
						if($columnas==2){$letracont='B';}
						if($columnas==3){$letracont='C';}
						if($columnas==4){$letracont='D';}
						if($columnas==5){$letracont='E';}
						if($columnas==6){$letracont='F';}
						
						if ($columnas == 1){
							$categoria = null;
							$id=$worksheet->getCell($letracont . $rowIndex)->getCalculatedValue();
							if(isset($id) or $id!='')
							{
								$categoria = $em->getRepository('GESTIONGestionBundle:Categoria')->find($id);
							}
						}
						if ($columnas == 2){
							$codigo=$worksheet->getCell($letracont . $rowIndex)->getCalculatedValue();
							if(empty($codigo)){
								break;
							}
							
							$elemento = $em->getRepository('GESTIONGestionBundle:Elemento')->findOneBy(array('codigo' => $codigo));
							if(is_null($elemento)){
								$elemento = new Elemento();
								$elemento->setCodigo($codigo);
								if(!is_null($categoria)){
									$elemento->setCategoria($categoria);
								}
								$existe = false;
							}else{
								if(!is_null($categoria)){
									$elemento->setCategoria($categoria);
								}
							}
						}
						if ($columnas == 3){
							$stock = $worksheet->getCell($letracont . $rowIndex)->getCalculatedValue();
						}
						if ($columnas == 4){
							$costo = str_replace(',', '.', $worksheet->getCell($letracont . $rowIndex)->getCalculatedValue());
						}
						if ($columnas == 5){
							$elemento->setNombre($worksheet->getCell($letracont . $rowIndex)->getCalculatedValue());
						}
						if ($columnas == 6){
							$elemento->setPrecio(str_replace(',', '.', $worksheet->getCell($letracont . $rowIndex)->getCalculatedValue()));							
							$em->persist($elemento);
        					$em->flush();

							$elementostock = $em->getRepository('GESTIONGestionBundle:ElementoStock')->findOneBy(array('costo' => $costo, 'elemento'=>$elemento->getId()));
							if(is_null($elementostock)){
								$hoy = new \DateTime('NOW');
								$elementostock = new ElementoStock();
								$elementostock->setTipofactura("CM");
								$elementostock->setFactura($hoy->format('Ymd'));
								$elementostock->setElemento($elemento);
								$elementostock->setCosto($costo);
								$elementostock->setStock($stock);
								$elementostock->setFecha($hoy);
								$em->persist($elementostock);
								$em->flush();
							}else{
								$stockActual = $elementostock->getStock() + $stock;
								$elementostock->setStock($stockActual);
							}							
						}
					}						
				}
				
				$control = $worksheet->getCell('B'.$i)->getCalculatedValue();
				if(empty($control)){
					$vacio = true;
				}

				$cacheDriver = new \Doctrine\Common\Cache\ArrayCache();
				$deleted = $cacheDriver->deleteAll();			
			}
			// ENVIAR DATOS
			if(!$vacio){
				$this->sessionManager->setSession('desde', $b);	 			
				$this->sessionManager->setSession('fin', 'no');	 			
				$this->sessionManager->setSession('uploadfile', $uploadfile);
				$this->sessionManager->addFlash('msgWarn', 'Se ha actualizado desde la linea '.$a.' hasta la linea '.$b.'. Y continuando...');
				return $this->redirect($this->generateUrl('_inicio'));
			}else{
				$this->sessionManager->addFlash('msgOk', 'Carga satisfactora. Ultima linea: '.$i);
				$this->sessionManager->setSession('desde', '');	 			
				$this->sessionManager->setSession('fin', '');	 			
				$this->sessionManager->setSession('uploadfile', '');	 			
			}
		}	
		return $this->redirect($this->generateUrl('_cargamasiva'));
    }
}
?>
