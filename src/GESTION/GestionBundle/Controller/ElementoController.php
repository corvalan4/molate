<?php

namespace GESTION\GestionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\Common\Collections\ArrayCollection;

use GESTION\GestionBundle\Entity\Elemento;
use GESTION\GestionBundle\Entity\Resize;
use GESTION\GestionBundle\Form\ElementoType;
use GESTION\GestionBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

// IMPORTACION NECESARIA PARA ARCHIVOS
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Elemento controller.
 *
 */
class ElementoController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;
    /**
     * Lists all Elemento entities.
     *
     */
    public function indexAction()
    {
		ini_set('memory_limit','256M');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GESTIONGestionBundle:Elemento')->findAll();

        return $this->render('GESTIONGestionBundle:Elemento:index.html.twig', array(
            'entities' => $entities,
        ));
    }
	
    public function listadoAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GESTIONGestionBundle:Elemento')->findAll();

        return $this->render('GESTIONGestionBundle:Elemento:listado.html.twig', array(
            'entities' => $entities,
        ));
    }
	
    public function listadocategoriaAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GESTIONGestionBundle:Elemento')->listadocategoria($this->sessionManager->getSession('iddependencia'));
		
		$listado = new ArrayCollection();
		
		foreach($entities as $entity){
			$categoria= $em->getRepository('GESTIONGestionBundle:Categoria')->find($entity['id']);
			$entidad = array('stock'=>$entity['stock'], "nombrecategoria"=>$categoria->getNombre());
			$listado->add($entidad);
		}
		
        return $this->render('GESTIONGestionBundle:Elemento:listadocategoria.html.twig', array(
            'entities' => $listado,
        ));
    }
    /**
     * Creates a new Elemento entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Elemento();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
		$em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {

			$file = $form['imagen']->getData();
			if($file){
				if($file->getSize()>0){
					$imgredimencionada = new Resize();
					$imgredimencionada->max_width(800);
					$imgredimencionada->max_height(800);
					$imgredimencionada->image_path($file->getRealPath());
					$imgredimencionada->image_resize();				
					$strm = fopen($file->getRealPath(),'rb');
					$entity->setImagen('data:'.$file->getClientMimeType().'/'.$file->getClientOriginalExtension().';base64,'.base64_encode(stream_get_contents(($strm))));
				}
			}

			$file = $form['imagendos']->getData();			
			if(!empty($file) and $file->getSize()>0){
				$imgredimencionada = new Resize();
				$imgredimencionada->max_width(800);
				$imgredimencionada->max_height(800);
				$imgredimencionada->image_path($file->getRealPath());
				$imgredimencionada->image_resize();				
				$strm = fopen($file->getRealPath(),'rb');
				$entity->setImagendos('data:'.$file->getClientMimeType().'/'.$file->getClientOriginalExtension().';base64,'.base64_encode(stream_get_contents(($strm))));
			}

			$file = $form['imagentres']->getData();			
			if(!empty($file) and $file->getSize()>0){
				$imgredimencionada = new Resize();
				$imgredimencionada->max_width(800);
				$imgredimencionada->max_height(800);
				$imgredimencionada->image_path($file->getRealPath());
				$imgredimencionada->image_resize();				
				$strm = fopen($file->getRealPath(),'rb');
				$entity->setImagentres('data:'.$file->getClientMimeType().'/'.$file->getClientOriginalExtension().';base64,'.base64_encode(stream_get_contents(($strm))));
			}

            $entity->setSlug($this->sessionManager->getSlug($entity->getNombre()));
			$em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('elemento'));
        }

        return $this->render('GESTIONGestionBundle:Elemento:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Elemento entity.
     *
     * @param Elemento $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Elemento $entity)
    {
        $form = $this->createForm(new ElementoType($this->sessionManager->getSession('iddependencia')), $entity, array(
            'action' => $this->generateUrl('elemento_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Elemento entity.
     *
     */
    public function newAction()
    {
        $entity = new Elemento();
        $form   = $this->createCreateForm($entity);

        return $this->render('GESTIONGestionBundle:Elemento:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Elemento entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);
		
//		$image = stream_get_contents($entity->getImagen());
		
        return $this->render('GESTIONGestionBundle:Elemento:show.html.twig', array(
            'entity'      => $entity,
//			'imagen'      => $image,
        ));
    }

    /**
     * Displays a form to edit an existing Elemento entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Elemento entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GESTIONGestionBundle:Elemento:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Elemento entity.
    *
    * @param Elemento $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Elemento $entity)
    {
        $form = $this->createForm(new ElementoType($this->sessionManager->getSession('iddependencia')), $entity, array(
            'action' => $this->generateUrl('elemento_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }
    /**
     * Edits an existing Elemento entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

 		// $fechastr = $editForm->get("vencimiento")->getData();
		// if(!empty($fechastr)){
			// $entity->setVencimiento(new \DateTime($fechastr));
		// }
		if ($editForm->isValid()) {
			$file = $editForm['imagen']->getData();			
			if(!empty($file) and $file->getSize()>0){
				$imgredimencionada = new Resize();
				$imgredimencionada->max_width(800);
				$imgredimencionada->max_height(800);
				$imgredimencionada->image_path($file->getRealPath());
				$imgredimencionada->image_resize();				
				$strm = fopen($file->getRealPath(),'rb');
				$entity->setImagen('data:'.$file->getClientMimeType().'/'.$file->getClientOriginalExtension().';base64,'.base64_encode(stream_get_contents(($strm))));
				
			}

			$file = $editForm['imagendos']->getData();			
			if(!empty($file) and $file->getSize()>0){
				$imgredimencionada = new Resize();
				$imgredimencionada->max_width(800);
				$imgredimencionada->max_height(800);
				$imgredimencionada->image_path($file->getRealPath());
				$imgredimencionada->image_resize();				
				$strm = fopen($file->getRealPath(),'rb');
				$entity->setImagendos('data:'.$file->getClientMimeType().'/'.$file->getClientOriginalExtension().';base64,'.base64_encode(stream_get_contents(($strm))));
			}

			$file = $editForm['imagentres']->getData();			
			if(!empty($file) and $file->getSize()>0){
				$imgredimencionada = new Resize();
				$imgredimencionada->max_width(800);
				$imgredimencionada->max_height(800);
				$imgredimencionada->image_path($file->getRealPath());
				$imgredimencionada->image_resize();				
				$strm = fopen($file->getRealPath(),'rb');
				$entity->setImagentres('data:'.$file->getClientMimeType().'/'.$file->getClientOriginalExtension().';base64,'.base64_encode(stream_get_contents(($strm))));
			}

            $entity->setSlug($this->sessionManager->getSlug($entity->getNombre()));
            $em->flush();

			return $this->redirect($this->generateUrl('elemento'));
       }

        return $this->render('GESTIONGestionBundle:Elemento:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a Elemento entity.
     *
     */
	public function deleteAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);
		$entity->setCodEstado("E");
		$em->flush();

        return $this->redirect($this->generateUrl('elemento'));
    }

    public function activarAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);
		$entity->setCodEstado("A");
		$em->flush();

        return $this->redirect($this->generateUrl('elemento'));
    }
	
	public function novisibleAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);
		$entity->setVisible(false);
		$em->flush();

        return $this->redirect($this->generateUrl('elemento'));
    }

    public function visibleAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);
		$entity->setVisible(true);
		$em->flush();

        return $this->redirect($this->generateUrl('elemento'));
    }

    /**
     * Creates a form to delete a Elemento entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('elemento_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
