<?php

namespace GESTION\GestionBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use GESTION\GestionBundle\Entity\ElementoStock;
use GESTION\GestionBundle\Form\ElementoStockType;
use GESTION\GestionBundle\Services\SessionManager;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * ElementoStock controller.
 *
 */
class ElementoStockController extends Controller
{
	/**
	 * @var SessionManager
	 * @DI\Inject("session.manager")
	 */
	public $sessionManager;

    /**
     * Lists all ElementoStock entities.
     *
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GESTIONGestionBundle:ElementoStock')->findBy(array('elemento'=>$id));
        $elemento = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);

        return $this->render('GESTIONGestionBundle:ElementoStock:index.html.twig', array(
            'entities' => $entities,
            'elemento' => $elemento,
        ));
    }
    /**
     * Creates a new ElementoStock entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ElementoStock();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
		$em = $this->getDoctrine()->getManager();

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('elementostock', array('id' => $entity->getElemento()->getId())));
        }

        return $this->render('GESTIONGestionBundle:ElementoStock:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ElementoStock entity.
     *
     * @param ElementoStock $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ElementoStock $entity)
    {
        $form = $this->createForm(new ElementoStockType(), $entity, array(
            'action' => $this->generateUrl('elementostock_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new ElementoStock entity.
     *
     */
    public function newAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $elemento = $em->getRepository('GESTIONGestionBundle:Elemento')->find($id);
        $entity = new ElementoStock();
        $entity->setElemento($elemento);
		$form   = $this->createCreateForm($entity);

        return $this->render('GESTIONGestionBundle:ElementoStock:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ElementoStock entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:ElementoStock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ElementoStock entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GESTIONGestionBundle:ElementoStock:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ElementoStock entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:ElementoStock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ElementoStock entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GESTIONGestionBundle:ElementoStock:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a ElementoStock entity.
    *
    * @param ElementoStock $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ElementoStock $entity)
    {
        $form = $this->createForm(new ElementoStockType(), $entity, array(
            'action' => $this->generateUrl('elementostock_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Modificar', 'attr'=>array('class'=>'btn btn-primary')));

        return $form;
    }
    /**
     * Edits an existing ElementoStock entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GESTIONGestionBundle:ElementoStock')->find($id);

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('elementostock', array('id' => $entity->getElemento()->getId())));
        }

        return $this->render('GESTIONGestionBundle:ElementoStock:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
    /**
     * Deletes a ElementoStock entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GESTIONGestionBundle:ElementoStock')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ElementoStock entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('elementostock'));
    }

    /**
     * Creates a form to delete a ElementoStock entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('elementostock_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
